Number.prototype.random = function(min, max) {
  // Overloading -> If both params are not passed then use these defaults.
  if ((min === null) || (max === null)) {
    min = 1;
    max = 10;
  }

  return Math.round(Math.random() * (max - min)) + min;
};