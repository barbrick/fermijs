//-------------------------------------------- constructor method
function TextboxValidator(myTargetId, myColor) {
  this.regExp = /\w/;
  this.errorColor = "#FF0000";
  this.target = document.getElementById(myTargetId);
  
  // Overloading constructor
  if (myColor) this.errorColor = myColor;
  
  //-------------------------------------------- get/set methods
  this.setRegExp = function(regExp) {
    this.regExp = regExp;
  };

  this.setErrorColor = function(errColor) {
    this.errorColor = errColor;
  };
  
  this.getTarget = function() {
    return this.target;
  };
  //-------------------------------------------- public methods
  this.check = function() {
    if(this.regExp.test(this.target.value)) {
      this.target.style.borderColor = "#00FF00";
      return true;
    } else {
      this.target.style.borderColor = this.errorColor;
      return false;
    }
  };
}