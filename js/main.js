function loaded() {
  var fermi = new Fermi("txtOutput");
  
  var val1 = new TextboxValidator("numOne");
  val1.setRegExp(/^\d$/);
  val1.getTarget().oninput = function() { val1.check(); };
  var val2 = new TextboxValidator("numTwo");
  val2.setRegExp(/^\d$/);
  val2.getTarget().oninput = function() { val2.check(); };
  var val3 = new TextboxValidator("numThree");
  val3.setRegExp(/^\d$/);
  val3.getTarget().oninput = function() { val3.check(); };


  document.getElementById("btnReset").onclick = function() {
    document.getElementById("btnReset").disabled = true;
    document.getElementById("btnGuess").disabled = false;
    document.getElementById("gameForm").reset();
    fermi.reset();
  };

  document.getElementById("btnGuess").onclick = function() {
    if ((val1.check()) && (val2.check()) && (val3.check())) {
      document.getElementById("txtError").innerHTML = "";
      if (fermi.guess(val1.getTarget().value, val2.getTarget().value, val3.getTarget().value)) {
        document.getElementById("btnReset").disabled = false;
        document.getElementById("btnGuess").disabled = true;
      }
    } else document.getElementById("txtError").innerHTML = "Error, please check input.";
    event.preventDefault();
  };

  fermi.reset();
}