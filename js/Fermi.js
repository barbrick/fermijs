//-------------------------------------------- constructor method
function Fermi(outputTarget) {
  
  this.win = false;
  this.random = 0;
  this.guesses = [];
  this.target = [];
  this.output = null;

  //-------------------------------------------- get/set methods
  this.setTarget = function(regExp) {
    var tar1, tar2, tar3;
    // Generate three random numbers
    tar1 = this.random.random(0, 9);
    
    // Ensure second number is unique
    tar2 = this.random.random(0, 9);
    while (tar1 == tar2) tar2 = this.random.random(0, 9);
    
    // Ensure third number is unique
    tar3 = this.random.random(0, 9);
    while (tar3 == tar1 || tar3 == tar2) tar3 = this.random.random(0, 9);

    this.target = new Array(tar1, tar2, tar3);
    
    // DEBUG
    console.log("DEBUG : " + this.target[0] + "-" + this.target[1] + "-" + this.target[2]);
  };

  //-------------------------------------------- public methods
  this.reset = function() {
    if (!this.output) this.output = document.getElementById(outputTarget);
    // Reset game variables
    this.win = false;
    this.guesses = [];
    this.target = [];
    this.output.innerHTML = "<br>Welcome to Fermi !<br><br><br>The object of the game is to guess three numbers each between 0 and 9 in the correct order.<br><br> You will be guided by hints consisting of Fermi, Pico, and Nano.<br><br> Fermi - Indicates a digit is correct and in the correct position.<br><br> Pico -  Indicates a digit is correct, but in the wrong position.<br><br> Nano - Indicates a digit is incorrect.<br><br> The hints appear in alphabetical order, for example, if the target number was 467 and you guess 347 your hint will be Fermi Pico Nano.<br><br> The game ends when you reach three Fermi's indicating you have guessed the correct three digit combination. Try and guess the correct combination in the least number of guesses!<br><br><br> Good Luck !<br>";
    // Set a new target
    this.setTarget();
  };

  this.check = function(guess) {
    var passed = true;
    // Check against repeat guesses
    this.guesses.forEach(function(v, i, a) {
        if (v == guess) {
          passed = false;
        }
    });
    return passed;
  };

  this.guess = function(one, two, three) {
    if (this.guesses.length === 0) this.output.innerHTML = "";
    var guess = one.toString() + two.toString() + three.toString();
    if (this.check(guess)) {
      // Record guess
      this.guesses.push(guess);

      var fermiCount = 0, picoCount = 0, nanoCount = 0;

      // Check numbers
      // Check first target number
      if (this.target[0] == one) fermiCount+=1;
      else if ((this.target[0] == two) || (this.target[0] == three)) picoCount+=1;
      else nanoCount+=1;
      
      // Check second target number
      if (this.target[1] == two) fermiCount+=1;
      else if ((this.target[1] == one) || (this.target[1] == three)) picoCount+=1;
      else nanoCount+=1;
      
      // Check third target number
      if (this.target[2] == three) fermiCount+=1;
      else if ((this.target[2] == two) || (this.target[2] == one)) picoCount+=1;
      else nanoCount+=1;

      // Generate out
      // Check if the guess was a win
      if (fermiCount == 3) {
        var plural = "";
        if (this.guesses.length != 1) plural = "es";
        this.output.innerHTML +=  this.target[0] + " - " + this.target[1] + " - " + this.target[2] + " : " + 
                                  "Fermi" + " " + "Fermi" + " " + "Fermi" + "<br>" +
                                  "Congratulations, you guessed correctly in only " + this.guesses.length + " guess" + plural + "!<br>";
        return true;
      }
        
        // Generate hint
        else {
            // Build fermi string
            switch (fermiCount) {
                case 0: fermi = "";
                        break;
                case 1: fermi = "Fermi";
                        break;
                case 2: fermi = "Fermi" + " " + "Fermi";
                        break;
                case 3: fermi = "Fermi" + " " + "Fermi" + " " + "Fermi";
                        break;
                default: fermi = " ERROR ";
                         break;
           }
            
           // Build pico string
            switch (picoCount) {
                case 0: pico = "";
                        break;
                case 1: pico = " " + "Pico";
                        break;
                case 2: pico = " " + "Pico" + " " + "Pico";
                        break;
                case 3: pico = "Pico" + " " + "Pico" + " " + "Pico";
                        break;
                default: pico = " ERROR ";
                         break;
           }
            
            // Build nano string
            switch (nanoCount) {
                case 0: nano = "";
                        break;
                case 1: nano = " " + "Nano";
                        break;
                case 2: nano = " " + "Nano" + " " + "Nano";
                        break;
                case 3: nano = "Nano" + " " + "Nano" + " " + "Nano";
                        break;
                default: nano = " ERROR ";
                         break;
           }
           
            this.output.innerHTML += one + " - " + two + " - " + three + " : " + fermi + pico + nano + "<br>";
            return false;
        }

    } else {
      return false;
    }
  };
}